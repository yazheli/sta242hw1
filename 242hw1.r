files = list.files(path = "C:/statistics/242/hw1/data", recursive = TRUE) 
getwd()
setwd('C:/statistics/242/hw1/data')
library(ggplot2)

myfunction <- function(x){
  a=readLines(x)
  
  rownumber = grep ("=" , a)
  
  c=unlist(strsplit(a[rownumber-1]," "))
  c=toupper(c)
  c=c[grep(FALSE,c(c==""))]
  if(sum(grepl('/', c))>0){c[grep('/', c)]=paste(c[grep('/', c)-1],c[grep('/', c)]);
                           c=c[-(grep('/', c)-1)]}
  
  if(sum(grepl('TIM$',c))>0){d=rep(0,2);d=grep('TIM$',c);c[d[1]]=paste(c[d[1]-1],c[d[1]]);c[d[2]]=paste(c[d[2]-1],c[d[2]]);
                             c=c[-(grep('TIM$',c)-1)]}
  
  if(sum(grepl('MI$', c))>0){c[grep('MI$', c)]=paste(c[grep('MI$', c)-1],c[grep('MI$', c)]);
                           c=c[-(grep('MI$', c)-1)]}
  
  if(sum(grepl('KM$', c))>0){c[grep('KM$', c)]=paste(c[grep('KM$', c)-1],c[grep('KM$', c)]);
                             c=c[-(grep('KM$', c)-1)]}
  
  if(sum(grepl('MILE$', c))>0){c[grep('MILE$', c)]=paste(c[grep('MILE$', c)-1],c[grep('MILE$', c)]);
                             c=c[-(grep('MILE$', c)-1)]}
  
  s=unlist(strsplit(a[rownumber]," "))
  
  n=nchar(s)+1;n
  n=c(n)
  
  
  b = read.fwf(x,n,comment.char="",skip=rownumber)
  b=as.data.frame(b)
  
  q=0
  for(i in 1:ncol(b)) {if(sum(is.na(b[,i]))==nrow(b)) {q=i;break;}}
  if(q==0) b=b
  if(q>0) b=b[,-q]
  
  
  colnames(b)=c
  
  q=0
  for(i in 1:ncol(b)) {if(sum(grepl('^\\*$',b[,i]))>0) {q=i}}
  if(q==0) b=b
  if(q>0) {colnames(b)[q+1]=colnames(b)[q]; colnames(b)[q]='MARK'}
  
  if(sum(grepl("#|*",unlist(b[nrow(b)-(1),])))>0)  b=b[(-(nrow(b)-1)),]
  if(sum(grepl("#|*",unlist(b[nrow(b)-(2),])))>0)  b=b[(-(nrow(b)-2)),]
  if(sum(grepl("#|*",unlist(b[nrow(b)-(0),])))>0)  b=b[(-(nrow(b)-0)),]
  b = b[-nrow(b),]
  return (b)
}
##############################################################################

files=files[-15]
result = sapply(files, function(x) myfunction(x))

##############################################################################
#problem:men 2006 women 2001 2006
############################################################################
#for men and women 2006
men2006=myfunction('men10Mile_2006')
women2006=myfunction('women10Mile_2006')


variablename=unlist(c(strsplit(colnames(men2006)[1],split=' '),colnames(men2006)[2:length(colnames(men2006))]))
colnames(result[["men10Mile_2006"]])=variablename[-length(variablename)]
colnames(result[["women10Mile_2006"]])=variablename[-length(variablename)]

#for women 2001 borrow the variable name from men 2001
a=readLines('men10Mile_2001')

rownumber = grep ("=" , a)
s=unlist(strsplit(a[rownumber]," "))

n=nchar(s)+1;n
n=c(n)
c=unlist(strsplit(a[rownumber-1]," "))
c=toupper(c)
c=c[grep(FALSE,c(c==""))]

b = read.fwf('women10Mile_2001',n,comment.char="",skip=18)
b=as.data.frame(b)

colnames(b)=c

result[["women10Mile_2001"]] = b




#handle 09 man
m = readLines("men10Mile_2009", encoding = "UTF-8")
#change the UTF-8 space to regular space (for men10Mile_2009 file)
m = gsub("[\u00A0]", " ", m)

rownumber = grep ("=" , m)

s=unlist(strsplit(m[rownumber]," "))
n=nchar(s)+1;n
n=c(n)
c=unlist(strsplit(m[rownumber-1]," "))
c=toupper(c)
c=c[grep(FALSE,c(c==""))]
if(sum(grepl('/', c))>0){c[grep('/', c)]=paste(c[grep('/', c)-1],c[grep('/', c)]);
                         c=c[-(grep('/', c)-1)]}

if(sum(grepl('TIM$',c))>0){d=rep(0,2);d=grep('TIM$',c);c[d[1]]=paste(c[d[1]-1],c[d[1]]);c[d[2]]=paste(c[d[2]-1],c[d[2]]);
                           c=c[-(grep('TIM$',c)-1)]}

if(sum(grepl('MI$', c))>0){c[grep('MI$', c)]=paste(c[grep('MI$', c)-1],c[grep('MI$', c)]);
                           c=c[-(grep('MI$', c)-1)]}

if(sum(grepl('KM$', c))>0){c[grep('KM$', c)]=paste(c[grep('KM$', c)-1],c[grep('KM$', c)]);
                           c=c[-(grep('KM$', c)-1)]}

if(sum(grepl('MILE$', c))>0){c[grep('MILE$', c)]=paste(c[grep('MILE$', c)-1],c[grep('MILE$', c)]);
                             c=c[-(grep('MILE$', c)-1)]}




M = read.fwf(textConnection(m), width=n, skip=rownumber, comment.char = "")
M=M[,-9]
colnames(M)=c
result[["men10Mile_2009"]]=M

####################################################################################
# split #and*   men2000 2001 2006 2007 2009 2010, 2003
#             women2000 2001 2006 2007 2009 2010, 2003

#####################################################################################
View(result[["men10Mile_2007"]])


mark = function(x){

  MARK = rep(NA,nrow(result[[x]]))

  for(i in 1:nrow(result[[x]])){
  
  if(sum(grepl("#|\\*",as.matrix(result[[x]][i,])))>0)
  {
    loca=grep("#|\\*",as.matrix(result[[x]][i,]))
    
    a=as.matrix(result[[x]][i,][loca])
    a=as.character(a)
    b=substr(a, 1, nchar(a)-1)
    c=substr(a, nchar(a),nchar(a))
    MARK[i]=c
    }
  
}

result[[x]] = cbind(result[[x]],MARK)
return (result[[x]])
}


x=c("men10Mile_2000","men10Mile_2001","men10Mile_2006","men10Mile_2007","men10Mile_2009","men10Mile_2010",
    "women10Mile_2000","women10Mile_2001","women10Mile_2006","women10Mile_2007","women10Mile_2009","women10Mile_2010")

result = sapply(x, function(x) result[[x]]=mark(x))

View(result[["women10Mile_2007"]])

###################################################################################
#split/
myfunction2 = function(x){

if  (sum(grep("/",colnames(result[[x]])))>0)

{loc=grep("/",colnames(result[[x]]))
tt=strsplit(as.character(result[[x]][,loc]),split="/")
DIV=rep(NA,nrow(result[[x]]))
TOT=rep(NA,nrow(result[[x]]))
for(i in 1:nrow(result[[x]])) DIV[i]=tt[[i]][1]
for(i in 1:nrow(result[[x]])) TOT[i]=tt[[i]][2]
result[[x]] = cbind(result[[x]],DIV,TOT)
return(result[[x]])}
else {return(result[[x]])}
}



files = list.files(path = "C:/statistics/242/hw1/data", recursive = TRUE) 
result2 = sapply(files, function(x) myfunction2(x))
View(result2[["men10Mile_2007"]])

##################################################################################
#change time format
##################################################################################
temresult = result2


changetime=function(x){
  
  TIME=list()
  loc=grep(":",as.matrix(temresult[[x]][1,]))
  for (i in 1:nrow(temresult[[x]]))
  { 
    
    T=c(as.matrix(temresult[[x]][i,loc]))
    
    pattern="\\s+([0-9]*[0-9]+[[:punct:]]+[0-9]+)"  
    
    newT=gsub(pattern,"0:\\1",T)
    tt = strsplit(as.character(newT), ":|#|\\*")
    TIME[[i]] = sapply(tt, function( x ) sum(c(3600, 60, 1) *as.numeric(x) ))
    
  }
  
  TIME1=matrix(numeric(length(TIME)*length(loc)),ncol=length(loc))
  
  for (i in 1:length(TIME))
  
  TIME1[i,]=TIME[[i]]
  
  colnames(TIME1) = colnames(temresult[[x]])[loc]
  
  return (TIME1)
}


files = list.files(path = "C:/statistics/242/hw1/data", recursive = TRUE) 

TIME = sapply(files, function(x) changetime(x))


#####################################################################################
#merge in to a big data frame
#####################################################################################
#change variable name to the same format


time_name_change = function(x){
  a = colnames(TIME[x][[1]])
  a = gsub("GUN TIM","TIME",a)
  a = gsub("GUN","TIME",a)
  return (a)
  }



for (x in files)
  {colnames(TIME[x][[1]])=time_name_change(x)}

#add year and sex
sexyear=strsplit(split="10Mile_",files)

for (i in 1:24)
TIME[[i]]=cbind(TIME[[i]],rep(sexyear[[i]][1],nrow(TIME[[i]])),rep(sexyear[[i]][2],nrow(TIME[[i]])))

for (i in 1:24){

length=length(names(TIME[[i]][1,]))
colnames(TIME[[i]])[length-1] = "SEX"
colnames(TIME[[i]])[length] = "YEAR"
}

NEWTIME=list()
for (i in 1:24)
NEWTIME[[i]] = as.data.frame(TIME[[i]])

#merge
merge = function(x){
tt = matrix(NA,ncol=12,nrow=nrow(TIME[[x]]))
tt = as.data.frame(tt)
colnames(tt)=c("PLACE", "DIV", "TOT", "NAME","AG", "HOMETOWN", "GUIDTYPE",  "S",'MARK',"YEAR","SEX","TIME")

pattern1=c("YEAR","SEX","TIME")
pattern2=c("PLACE", "^DIV$", "^TOT$", "NAME","AG", "HOMETOWN", "GUIDTYPE",  "S",'MARK')

for (i in 1:9){

if (sum(grepl(pattern2[i],colnames(result2[[x]])))>0) tt[,i]=result2[[x]][,grep(pattern2[i],colnames(result2[[x]]))]
}

for (j in 1:3) { 
  tt[,9+j]=NEWTIME[[x]][,grep(pattern1[j],colnames(NEWTIME[[x]]))]
}
return (tt)
}

result3=list()
for (i in 1:24)
  result3[[i]]=merge(i)

finalresult = do.call(rbind,result3)

#change format
time = as.matrix(finalresult$TIME)
time = apply(time,2,as.numeric)
time[time > 10000]=NA
time[time < 2000]=NA
finalresult$TIME = time
time = as.data.frame(time)
time$year=unlist(ye[,1])
time$sex=finalresult$SEX
time=time[-which(time$year==2003),]
ggplot(time, aes(factor(year), V1))+ geom_boxplot(aes(fill = sex))+
  ylab("time")+xlab("year")+ggtitle("RunTime over years: Gender")

place = as.matrix(finalresult$PLACE)
place = apply(place,2,as.numeric)
finalresult$PLACE = place

age = as.matrix(finalresult$AG)
age = apply(age,2,as.numeric)
finalresult$AG = age

age = as.data.frame(age)
age$year=unlist(ye[,1])
age$sex=finalresult$SEX
ggplot(age, aes(factor(year), V1))+ geom_boxplot(aes(fill = sex))+
  ylab("age")+xlab("year")+ggtitle("age over years:sex")


#create a variable unique ID use name's first three character and born year
library(stringr)
name = str_trim(finalresult$NAME,side='both')

ye = as.matrix(finalresult$YEAR)
ye = apply(ye,2,as.numeric)
born = ye-finalresult$AG

ID = toupper(substr(name, 1, 20))
ID = paste0(ID,born)
finalresult$ID = ID

###########################################################################3
#analysis
#how many people particapate in the march in each year

num_people = table(finalresult$YEAR)
plot(num_people,main = "num of people in each year",type='l')
#from the plot we find the number of people continuing increasing in the decades, and it is
#three times of people in 2010 compare to number of people in 1999

#consider about sex
num_women = table(finalresult$YEAR[finalresult$SEX=='women'])
num_men = table(finalresult$YEAR[finalresult$SEX=='men'])

plot(num_women,main="num of people in different sex",type='l',col='red',ylab="num")
lines(num_men,type='l',col='blue')
legend('topleft' , legend = c('Male' , 'Female') , lty = c(1 , 1 ) , col = c('blue' , 'red' ),cex=.5)

#from the plot,obviously the number of male and female particapation continuous increase in
#decades. And it is very interesting that before 2005 there are more male particapations,
#but after 2005 there are more male particapations.

# the mean of TIME in decades
time = as.matrix(finalresult$TIME)
time = apply(time,2,as.numeric)
time[time > 18000]=NA
time[time < 2000]=NA


finalresult$TIME = time
plot(density(time,na.rm=TRUE),xlim=c(2000,10000),main='histgram of time',xlab='time/s',ylab='Density')

qplot(time, data=as.data.frame(time), geom="histogram",xlim=c(2000,10000),main='histgram of time',xlab='time/s',ylab='Density')

plot(density(finalresult$TIME[finalresult$SEX=='men'],na.rm=TRUE),xlim=c(2000,10000),main='histgram of men and women time',col='blue',xlab='time/s',ylab='Density')
lines(density(finalresult$TIME[finalresult$SEX=='women'],na.rm=TRUE),xlim=c(2000,10000),main='histgram of women time',col='red',xlab='time/s',ylab='Density')
legend('topright' , legend = c('Male' , 'Female') , lty = c(1 , 1 ) , col = c('blue' , 'red' ),cex=.5)

ggplot(dat, aes(x=rating, fill=cond)) +
  geom_histogram(binwidth=.5, alpha=.5, position="identity")

sex=levels(finalresult$SEX)
year=levels(finalresult$YEAR)

yeartime = rep(0,12)
sdtime = rep(0,12)

for (i in 1:length(year)){
  yeartime[i] = mean(finalresult$TIME[finalresult$YEAR==year[i]],na.rm=TRUE)
  sdtime[i] = sd(finalresult$TIME[finalresult$YEAR==year[i]],na.rm=TRUE)
}
names(yeartime)=year
names(sdtime)=year
plot(year,yeartime,type='l',main = 'mean of time in each year',ylab='time/s')
plot(year,sdtime,type='l')

#it seems the mean of time was incresing, why? is it because of the number of women
#was increasing?

#in order to answer this problem first we calcuate the ratio of the women particapations in
#decades
womenratio = num_women/(num_women+num_men)
cov(womenratio,yeartime)/(sd(womenratio)*sd(yeartime))
#the correlation coefficient is 0.5, it seems it is revelant

#let us talk about something about place
place = as.matrix(finalresult$PLACE)
place = apply(place,2,as.numeric)
finalresult$PLACE = place
finalresult$PLACE[finalresult$YEAR==2009]

#we want to have the champion's time in each year
champion=finalresult[which(finalresult$PLACE==1),]
plot(year[-2],champion$TIME[champion$SEX=='men'],type='l',ylab='time/s',xlab='year',main='man champion competition result')
plot(year,champion$TIME[champion$SEX=='women'],type='l',ylab='time/s',xlab='year',main='woman champion competition result')
#it seems there is no huge difference in different year's champions time.

#next we consider about people who Under USATF OPEN guideline and Under USATF Age-Group guideline
OPENguide = finalresult[which(finalresult$MARK=='#'),]
AgeGroupguide = finalresult[which(finalresult$MARK=='*'),]
OPENguide=OPENguide[which((OPENguide$TIME=='NA')==FALSE),]
AgeGroupguide=AgeGroupguide[which((AgeGroupguide$TIME=='NA')==FALSE),]

table(OPENguide$YEAR[OPENguide$SEX=='men'])
table(AgeGroupguide$YEAR[AgeGroupguide$SEX=='men'])
table(OPENguide$YEAR[OPENguide$SEX=='women'])
table(AgeGroupguide$YEAR[AgeGroupguide$SEX=='women'])

#focus on age now
age = as.matrix(finalresult$AG)
age = apply(age,2,as.numeric)
finalresult$AG = age

boxplot(age[finalresult$YEAR == year[1]])

plot(density(age,na.rm=TRUE),main='histgram of age',xlab='age',ylab='Density')


plot(density(finalresult$AG[finalresult$SEX=='men'],na.rm=TRUE),main='histgram of men and women age',col='blue',xlab='age',ylab='Density',ylim=c(0,0.06))
lines(density(finalresult$AG[finalresult$SEX=='women'],na.rm=TRUE),col='red',xlab='age',ylab='Density')
legend('topright' , legend = c('Male' , 'Female') , lty = c(1 , 1 ) , col = c('blue' , 'red' ),cex=.5)

meanage = rep(0,12)
sdage = rep(0,12)
for (i in 1:length(year)){
  meanage[i] = mean(finalresult$AG[finalresult$YEAR==year[i]],na.rm=TRUE)
  sdage[i] = sd(finalresult$AG[finalresult$YEAR==year[i]],na.rm=TRUE)
}
plot(year,meanage,main='age in each year',ylab='age',ylim=c(30,45),type='l')
plot(year,sdage,main='age sd in each year',ylab='',ylim=c(9,12))

#analysis of unique ID
frequency=table(finalresult$ID)
frequency=sort(frequency,decreasing=TRUE)
names(which(frequency==12))
names(which(frequency==11))
names(which(frequency==10))

plot(year,finalresult[which(finalresult$ID == 'NIANXIANG XIE1928'),]$TIME,
     type='l',xlab='year',ylab='time',main='NIANXIANG XIE result')

plot(year[-5],finalresult[which(finalresult$ID == 'PAT WELCH1945'),]$TIME[-5],
     type='l',xlab='year',ylab='time',main='PAT WELCH result')

finalresult[finalresult$ID == 'NIANXIANG XIE1928',]

#analysis of hometown
library(stringr)
hometown = str_trim(finalresult$HOMETOWN,side='both')
hometown=strsplit(hometown,split=' ')
i = c(1:length(hometown))
fstate = function(x) hometown[[x]][length(hometown[[x]])]
state = sapply(i, function(i) fstate(i) )
state = unlist(state)
us.state = c( "AA","AE","AP","AL","AK","AS","AZ","AR","CA","CO","CT",
              "DE","DC","FM","FL","GA","GU","HI","ID","IL","IN","IA",
              "KS","KY","LA","ME","MH","MD","MA","MI","MN","MS","MO",
              "MT","NE","NV","NH","NJ","NM","NY","NC","ND","MP","OH",
              "OK","OR","PW","PA","PR","RI","SC","SD","TN","TX","UT",
              "VT","VI","VA","WA","WV","WI","WY")
table = table(state)
inttable = table[!names( table)%in%us.state]
table = table[names( table)%in%us.state]

others = sum(table[table<1000])
state = as.matrix(c(table[table>1000],others))
rownames(state)[7]='others'
pie(state, main="State Piechart", col=rainbow(nrow(state)),
    labels=c("DC","MD","NJ","NY","PA","VA","others"))

#the regressinon between age and time
timeage = cbind(as.matrix(finalresult$AG),as.matrix(finalresult$TIME))
timeage=as.data.frame(timeage)
colnames(timeage)=c('age','time')
fit = lm(time~age,data=timeage)
plot(timeage$age,timeage$time, main='Data and fitted line',xlab='age',ylab='time')
abline(fit, col='red')
anova(fit)
summary(fit)
############################################################

x="women10Mile_2000"
result[[x]]=mark(x)
x="women10Mile_2001"
result[[x]]=mark(x)
x="women10Mile_2006"
result[[x]]=mark(x)
x="women10Mile_2007"
result[[x]]=mark(x)
x="women10Mile_2009"
result[[x]]=mark(x)
x="women10Mile_2010"
result[[x]]=mark(x)
plot(density(final.table$Gun_Time[final.table$GENDER == 'Male'] , na.rm=TRUE) , xlim = c(2000 , 10000) ,
     main = "Density plot of Gun Time" ,col='blue',xlab='time/s',ylab='Density')
lines(density(final.table$Gun_Time[final.table$GENDER == 'Female'] , na.rm=TRUE) , col='red')
lines(density(final.table$Gun_Time , na.rm=TRUE))
legend('topright' , legend = c('Male' , 'Female' , 'Total') , lty = c(1 , 1 , 1) , col = c('blue' , 'red' , 'black'))